#include <cstdio>
#include <QApplication>
#include <QLabel>
#include <QMainWindow>
#include <QScrollArea>

#include "wad.h"
#include "mapdiff.h"

int main(int argc, char* argv[])
{
	if(argc < 4)
	{
		printf("Usage: mapdiff <old> <new> <map> [<map2>]\n");
		return 0;
	}

	QApplication app(argc, argv);

	WadFile oldFile(argv[1]);
	WadFile newFile(argv[2]);
	if(!oldFile.open(QIODevice::ReadOnly) ||
		!newFile.open(QIODevice::ReadOnly))
	{
		printf("One of the files failed to open.\n");
		return 0;
	}

	printf("Map found, running comparision!\n");

	MapDiff diff(&oldFile, &newFile, argv[3], argc < 5 ? argv[3] : argv[4]);

	QMainWindow window;
	QScrollArea *scrollArea = new QScrollArea();
	QLabel *mapWidget = new QLabel();
	diff.drawMap(mapWidget);
	scrollArea->setWidget(mapWidget);
	window.setCentralWidget(scrollArea);
	window.show();

	return app.exec();
}
