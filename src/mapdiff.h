#pragma once

class WadFile;
class QString;
class QLabel;

class MapDiff
{
public:
	MapDiff(WadFile *oldwad, WadFile *newwad, const QString &map, const QString &map2);
	~MapDiff();

	void drawMap(QLabel *device);
protected:
	void loadLines();
	void loadSectors();
	void loadSides();
	void loadThings();
	void loadVertexes();

private:
	struct Private;
	Private *d;
};
