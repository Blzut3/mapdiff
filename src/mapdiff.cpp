#include "mapdiff.h"
#include "wad.h"

#include <cassert>
#include <cstdio>
#include <QDataStream>
#include <QImage>
#include <QSet>
#include <QString>
#include <QPainter>
#include <QPoint>
#include <QLabel>
#include <QtCore/qmath.h>

struct Vertex
{
public:
	unsigned int num;
	qint16 x;
	qint16 y;

	// For "hashing"
	operator quint32() const { return (((quint32)(quint16)x)<<16)|(quint16)y; }
	bool operator==(const Vertex &other) const { return other.x == x && other.y == y; }
};

struct Linedef
{
public:
	quint16 v[2];
	quint16 flags;
	quint16 special;
	quint16 tag;
	quint16 side[2];

	quint64 getLocationHash(const QList<Vertex> &vert) const { return ((quint64)(quint32)vert[v[0]]<<32) | ((quint64)(quint32)vert[v[1]]); }
	bool operator==(const Linedef &other) const { return flags == other.flags && special == other.special && tag == other.tag; }
	inline bool operator!=(const Linedef &other) const { return !(*this == other); }
};

struct Sidedef
{
public:
	qint16 xoffset;
	qint16 yoffset;
	QString texture[3];
	qint16 sector;

	bool operator==(const Sidedef &other) const { return xoffset == other.xoffset && yoffset == other.yoffset && texture[0] == other.texture[0] && texture[1] == other.texture[1] && texture[2] == other.texture[2]; }
	inline bool operator!=(const Sidedef &other) const { return !(*this == other); }
};

struct Sector
{
public:
	qint16 height[2];
	QString texture[2];
	qint16 light;
	quint16 type;
	quint16 tag;

	bool marked;

	bool operator==(const Sector &other) const { return height[0] == other.height[0] && height[1] == other.height[1] && texture[0] == other.texture[0] && texture[1] == other.texture[1] && light == other.light && type == other.type && tag == other.tag; }
	inline bool operator!=(const Sector &other) const { return !(*this == other); }
};

struct Thing
{
	unsigned int num;

	qint16 x, y;
	quint16 angle;
	quint16 doomed;
	quint16 flags;

	operator quint32() const { return (((quint32)(quint16)x)<<16)|(quint16)y; }
	bool operator==(const Thing &other) const { return x == other.x && y == other.y; }

	bool isIdentical(const Thing &other) const { return x == other.x && y == other.y && angle == other.angle && doomed == other.doomed && flags == other.flags; }
};

struct MapDiff::Private
{
public:
	WadFile *oldwad, *newwad;
	WadLump oldmap, newmap;

	// Vertexes
	QList<Vertex> vertexList[2];
	QSet<Vertex> addedVertexes;
	QSet<Vertex> removedVertexes;
	QPoint upperLeft, lowerRight;

	// Things
	QList<Thing> thingList[2];
	QSet<Thing> addedThings;
	QSet<Thing> removedThings;

	QList<Linedef> linedefList[2];
	QList<Sidedef> sidedefList[2];
	QList<Sector> sectorList[2];
};

MapDiff::MapDiff(WadFile *oldwad, WadFile *newwad, const QString &map, const QString &map2) : d(new Private())
{
	d->oldwad = oldwad;
	d->newwad = newwad;

	d->oldmap = oldwad->lump(map);
	d->newmap = newwad->lump(map2);
	if(!d->oldmap.valid() || !d->newmap.valid())
	{
		printf("Map not present in one of the files.\n");
		return;
	}

	d->upperLeft = QPoint(65536, 65536);
	d->lowerRight = QPoint(-65536, -65536);

	loadVertexes();
	loadLines();
	loadSectors();
	loadSides();
	loadThings();
}

MapDiff::~MapDiff()
{
	delete d;
}

#include <QDebug>
void MapDiff::drawMap(QLabel *widget)
{
	unsigned int width = abs(d->lowerRight.x() - d->upperLeft.x()) + 1;
	unsigned int height = abs(d->lowerRight.y() - d->upperLeft.y()) + 1;
	int shiftx = -d->upperLeft.x();
	int shifty = -d->upperLeft.y();

	printf("Creating image of %dx%d (%d,%d) (%d,%d)\n", width, height, d->upperLeft.x(), d->upperLeft.y(), d->lowerRight.x(), d->lowerRight.y());
	QImage image(width, height, QImage::Format_RGB32);
	image.fill(0);

	QPainter painter(&image);

	// Draw the lines
	QHash<quint64, Linedef> commonLines;
	foreach(const Linedef &l, d->linedefList[0])
	{
		const Vertex &v1 = d->vertexList[0][l.v[0]];
		const Vertex &v2 = d->vertexList[0][l.v[1]];
		bool lineRemoved = d->removedVertexes.contains(v1) || d->removedVertexes.contains(v2);

		painter.setPen(lineRemoved ? QColor(255, 0, 0) : QColor(255, 255, 255));
		painter.drawLine(v1.x + shiftx, v1.y + shifty, v2.x + shiftx, v2.y + shifty);

		if(!lineRemoved)
			commonLines.insert(l.getLocationHash(d->vertexList[0]), l);
	}
	foreach(const Linedef &l, d->linedefList[1])
	{
		const Vertex &v1 = d->vertexList[1][l.v[0]];
		const Vertex &v2 = d->vertexList[1][l.v[1]];
		bool lineAdded = d->addedVertexes.contains(v1) || d->addedVertexes.contains(v2);

		painter.setPen(lineAdded ? QColor(0, 255, 0) : QColor(255, 255, 255));
		if(commonLines.contains(l.getLocationHash(d->vertexList[1])))
		{
			const Linedef &l2 = commonLines[l.getLocationHash(d->vertexList[1])];
			assert(l.getLocationHash(d->vertexList[1]) == l2.getLocationHash(d->vertexList[0]));
			assert(!lineAdded);
			if(l != l2)
			{
				assert(d->vertexList[0][l2.v[0]].x == d->vertexList[1][l.v[0]].x);
				printf("Line changed (%d, %d) (%d, %d)\n", v1.x, v1.y, v2.x, v2.y);
				if(l.flags != l2.flags) printf("\tflags %X -> %X\n", l2.flags, l.flags);
				if(l.special != l2.special) printf("\tspecial %u -> %u\n", l2.special, l.special);
				if(l.tag != l2.tag) printf("\ttag %u -> %u\n", l2.tag, l.tag);
				painter.setPen(QColor(0, 0, 255));
			}

			for(unsigned int s = 0;s < 2;++s)
			{
				if(l2.side[s] == 0xFFFF)
				{
					if(l.side[s] != 0xFFFF)
						printf("%s side added (%d, %d) (%d, %d)\n", s ? "Back" : "Front", v1.x, v1.y, v2.x, v2.y);
					continue;
				}

				const Sidedef &s1 = d->sidedefList[1][l.side[s]];
				const Sidedef &s2 = d->sidedefList[0][l2.side[s]];
				Sector &sec1 = d->sectorList[1][s1.sector];
				const Sector &sec2 = d->sectorList[0][s2.sector];
				if(sec2 != sec1 && !sec1.marked)
				{
					sec1.marked = true;

					printf("%s sector changed (%d, %d) (%d, %d)\n", s ? "Back" : "Front", v1.x, v1.y, v2.x, v2.y);
					if(sec2.height[0] != sec1.height[0]) printf("\tfloor height %d -> %d\n", sec2.height[0], sec1.height[0]);
					if(sec2.texture[0] != sec1.texture[0]) printf("\tfloor texture %s -> %s\n", sec2.texture[0].toAscii().constData(), sec1.texture[0].toAscii().constData());
					if(sec2.height[1] != sec1.height[1]) printf("\tceiling height %d -> %d\n", sec2.height[0], sec1.height[0]);
					if(sec2.texture[1] != sec1.texture[1]) printf("\tceiling texture %s -> %s\n", sec2.texture[1].toAscii().constData(), sec1.texture[1].toAscii().constData());
					if(sec2.light != sec1.light) printf("\tlight %d -> %d\n", sec2.light, sec1.light);
					if(sec2.type != sec1.type) printf("\ttype %d -> %d\n", sec2.type, sec1.type);
					if(sec2.tag != sec1.tag) printf("\ttag %d -> %d\n", sec2.tag, sec1.tag);

					QColor tmp = painter.pen().color();
					painter.setPen(QColor(255, 255, 0));
					int smx = (v1.x + v2.x)/2 + shiftx;
					int smy = (v1.y + v2.y)/2 + shifty;
					double angle = qAtan2(v1.x - v2.x, v1.y - v2.y);
					if((v1.x <= v2.x) ^ (v1.y < v2.y) ^ s)
						painter.drawLine(smx, smy, smx - 5*qCos(angle), smy + 5*qSin(angle));
					else
						painter.drawLine(smx, smy, smx + 5*qCos(angle), smy - 5*qSin(angle));
					painter.setPen(tmp);
				}

				if(s2 != s1)
				{
					printf("%s side changed (%d, %d) (%d, %d)\n", s ? "Back" : "Front", d->vertexList[1][l.v[0]].x, d->vertexList[1][l.v[0]].y, d->vertexList[1][l.v[1]].x, d->vertexList[1][l.v[1]].y);
					if(s2.xoffset != s1.xoffset) printf("\txoffset %d -> %d\n", s2.xoffset, s1.xoffset);
					if(s2.yoffset != s1.yoffset) printf("\tyoffset %d -> %d\n", s2.yoffset, s1.yoffset);
					if(s2.texture[0] != s1.texture[0]) printf("\tupper %s -> %s\n", s2.texture[0].toAscii().constData(), s1.texture[0].toAscii().constData());
					if(s2.texture[1] != s1.texture[1]) printf("\tmiddle %s -> %s\n", s2.texture[1].toAscii().constData(), s1.texture[1].toAscii().constData());
					if(s2.texture[2] != s1.texture[2]) printf("\tlower %s -> %s\n", s2.texture[2].toAscii().constData(), s1.texture[2].toAscii().constData());
					painter.setPen(QColor(255, 0, 255));
				}
			}
		}

		painter.drawLine(v1.x + shiftx, v1.y + shifty, v2.x + shiftx, v2.y + shifty);
	}

	// Draw the things
	QSet<Thing> commonThings = QSet<Thing>::fromList(d->thingList[1]) - d->removedThings;
	printf("Things removed %d, things added %d, things common %d\n", d->addedThings.count(), d->removedThings.count(), commonThings.count());
	painter.setBrush(QColor(0xFF, 0x00, 0x00));
	painter.setPen(QColor(0x99, 0x00, 0x00));
	foreach(const Thing &t, d->removedThings)
	{
		painter.drawEllipse(QPoint(t.x + shiftx, t.y + shifty), 3, 3);
	}
	foreach(const Thing &t, d->thingList[1])
	{
		if(d->addedThings.contains(t))
		{
			painter.setBrush(QColor(0x00, 0xFF, 0x00));
			painter.setPen(QColor(0x00, 0x99, 0x00));
		}
		else
		{
			if(commonThings.contains(t))
			{
				const Thing &t2 = *commonThings.find(t);
				if(!t2.isIdentical(t))
				{
					printf("Thing changed (%d, %d)\n", t.x, t.y);
					if(t2.angle != t.angle) printf("\tangle %X -> %X\n", t2.angle, t.angle);
					if(t2.doomed != t.doomed) printf("\tdoomed %u -> %u\n", t2.doomed, t.doomed);
					if(t2.flags != t.flags) printf("\tflags %X -> %X\n", t2.flags, t.flags);
					painter.setBrush(QColor(0x00, 0x00, 0xFF));
					painter.setPen(QColor(0x00, 0x00, 0x99));
				}
				else
				{
					painter.setBrush(QColor(0xFF, 0xFF, 0xFF));
					painter.setPen(QColor(0x99, 0x99, 0x99));
				}
			}
			else
			{
				painter.setBrush(QColor(0xFF, 0xFF, 0xFF));
				painter.setPen(QColor(0x99, 0x99, 0x99));
			}
		}
		painter.drawEllipse(QPoint(t.x + shiftx, t.y + shifty), 3, 3);
	}

	// Draw the vertexes
	foreach(const Vertex &v, d->vertexList[1])
	{
		image.setPixel(v.x + shiftx, v.y + shifty, 0xFFFFFF);
	}
	foreach(const Vertex &v, d->removedVertexes)
	{
		image.setPixel(v.x + shiftx, v.y + shifty, 0xFF0000);
	}
	foreach(const Vertex &v, d->addedVertexes)
	{
		image.setPixel(v.x + shiftx, v.y + shifty, 0x00FF00);
	}

	static bool doSave = true;
	if(doSave)
	{
		doSave = false;
		image.save("output.png");
	}
	widget->setPixmap(QPixmap::fromImage(image));
}

#define openlump(struct, ssize, lumpname, stream) \
	WadLump __lumps[2] = { \
		d->oldwad->lump(lumpname, d->oldmap), \
		d->newwad->lump(lumpname, d->newmap) \
	}; \
	for(unsigned int __i = 0;__i < 2; ++__i) \
	{ \
		__lumps[__i].open(QIODevice::ReadOnly); \
		QDataStream stream(&__lumps[__i]); \
		stream.setByteOrder(QDataStream::LittleEndian); \
		struct;\
		unsigned int __num = __lumps[__i].size()/ssize; \
		for(unsigned int __j = 0;__j < __num;++__j)
#define STRUCT_NUM __num
#define WAD_NUM __i
#define closelump() }

void MapDiff::loadLines()
{
	openlump(Linedef linedef, 14, "LINEDEFS", stream)
	{
		stream >>
			linedef.v[0] >> linedef.v[1] >>
			linedef.flags >> linedef.special >>
			linedef.tag >> linedef.side[0] >> linedef.side[1];
		d->linedefList[WAD_NUM].append(linedef);
	}
	closelump();
}

void MapDiff::loadSectors()
{
	openlump(Sector sector, 26, "SECTORS", stream)
	{
		sector.marked = false;

		char texture[2][9];
		memset(texture, 0, 2*9);

		stream >> sector.height[0] >> sector.height[1];
		stream.readRawData(texture[0], 8);
		sector.texture[0] = texture[0];
		stream.readRawData(texture[1], 8);
		sector.texture[1] = texture[1];
		stream >> sector.light >> sector.type >> sector.tag;

		d->sectorList[WAD_NUM].append(sector);
	}
	closelump();
}

void MapDiff::loadSides()
{
	QHash<QString, QString> textureRemap;
	// For Hacx - Normalize to 1.1 names
	textureRemap.insert("HD1", "BIGDOOR3");
	textureRemap.insert("HD2", "BIGDOOR5");
	textureRemap.insert("HD3", "BIGDOOR6");
	textureRemap.insert("HD4", "BIGDOOR7");
	textureRemap.insert("HD5", "DOORBLU2");
	textureRemap.insert("HD6", "DOORRED2");
	textureRemap.insert("HD7", "DOORYEL");
	textureRemap.insert("HD8", "DOORYEL2");
	textureRemap.insert("HW200", "BROVINE2");
	textureRemap.insert("HW201", "CEMENT1");
	textureRemap.insert("HW202", "CEMENT2");
	textureRemap.insert("HW203", "CEMENT3");
	textureRemap.insert("HW204", "CEMENT4");
	textureRemap.insert("HW205", "CEMENT5");
	textureRemap.insert("HW206", "CEMENT6");
	textureRemap.insert("HW207", "COMPBLUE");
	textureRemap.insert("HW208", "COMPWERD");
	textureRemap.insert("HW209", "CRATE1");
	textureRemap.insert("HW210", "CRATE2");
	textureRemap.insert("HW211", "CRATELIT");
	textureRemap.insert("HW212", "CRATINY");
	textureRemap.insert("HW213", "CRATWIDE");
	textureRemap.insert("HW214", "DOORSTOP");
	textureRemap.insert("HW215", "DOORTRAK");
	textureRemap.insert("HW216", "EXITDOOR");
	textureRemap.insert("HW217", "EXITSIGN");
	textureRemap.insert("HW218", "EXITSTON");
	textureRemap.insert("HW228", "GRAY1");
	textureRemap.insert("HW229", "GRAY2");
	textureRemap.insert("HW162", "GRAYBIG");
	textureRemap.insert("HW163", "GRAYPOIS");
	textureRemap.insert("HW164", "GRAYVINE");
	textureRemap.insert("HW165", "GSTGARG");
	textureRemap.insert("HW166", "GSTLION");
	textureRemap.insert("HW167", "GSTONE1");
	textureRemap.insert("HW168", "GSTONE2");
	textureRemap.insert("HW169", "GSTSATYR");
	textureRemap.insert("HW170", "GSTVINE1");
	textureRemap.insert("HW171", "GSTVINE2");
	textureRemap.insert("HW172", "ICKWALL1");
	textureRemap.insert("HW173", "ICKWALL2");
	textureRemap.insert("HW174", "ICKWALL3");
	textureRemap.insert("HW175", "ICKWALL4");
	textureRemap.insert("HW176", "ICKWALL5");
	textureRemap.insert("HW177", "ICKWALL7");
	textureRemap.insert("HW178", "MARBFAC2");
	textureRemap.insert("HW179", "MARBFAC3");
	textureRemap.insert("HW180", "MARBFACE");
	textureRemap.insert("HW181", "MARBLE1");
	textureRemap.insert("HW182", "MARBLE2");
	textureRemap.insert("HW183", "MARBLE3");
	textureRemap.insert("HW184", "MARBLOD1");
	textureRemap.insert("HW185", "METAL");
	textureRemap.insert("HW186", "MIDBRN1");
	textureRemap.insert("HW187", "MIDGRATE");
	textureRemap.insert("HW188", "NUKEPOIS");
	textureRemap.insert("HW191", "PIPE1");
	textureRemap.insert("HW192", "PIPE4");
	textureRemap.insert("HW193", "PIPE6");
	textureRemap.insert("HW194", "REDWALL");
	textureRemap.insert("HW198", "SHAWN1");
	textureRemap.insert("HW199", "SHAWN3");
	textureRemap.insert("HW300", "SKIN2");
	textureRemap.insert("HW301", "SKINCUT");
	textureRemap.insert("HW302", "SKINEDGE");
	textureRemap.insert("HW303", "SKINFACE");
	textureRemap.insert("HW304", "SKINLOW");
	textureRemap.insert("HW305", "SKINMET1");
	textureRemap.insert("HW306", "SKINMET2");
	textureRemap.insert("HW307", "SKINSCAB");
	textureRemap.insert("HW308", "SKINSYMB");
	textureRemap.insert("HW309", "SKSNAKE1");
	textureRemap.insert("HW310", "SKSNAKE2");
	textureRemap.insert("HW311", "SKSPINE1");
	textureRemap.insert("HW312", "SKSPINE2");
	textureRemap.insert("HW313", "SLADSKUL");
	textureRemap.insert("HW314", "SP_DUDE1");
	textureRemap.insert("HW315", "SP_DUDE2");
	textureRemap.insert("HW316", "SP_DUDE4");
	textureRemap.insert("HW317", "SP_DUDE5");
	textureRemap.insert("HW318", "SP_FACE1");
	textureRemap.insert("HW319", "SP_HOT1");
	textureRemap.insert("HW320", "SP_ROCK1");
	textureRemap.insert("HW321", "STARBR2");
	textureRemap.insert("HW322", "STARG2");
	textureRemap.insert("HW323", "STARGR2");
	textureRemap.insert("HW324", "STEPLAD1");
	textureRemap.insert("HW325", "STEPTOP");
	textureRemap.insert("HW326", "SUPPORT3");
	textureRemap.insert("HW327", "WOOD1");
	textureRemap.insert("HW328", "WOOD3");
	textureRemap.insert("HW329", "WOOD4");
	textureRemap.insert("HW330", "WOOD5");
	textureRemap.insert("HW331", "WOODGARG");

	openlump(Sidedef sidedef, 30, "SIDEDEFS", stream)
	{
		char texture[3][9];
		memset(texture, 0, 3*9);

		stream >> sidedef.xoffset >> sidedef.yoffset;
		for(unsigned int i = 0;i < 3;++i)
		{
			stream.readRawData(texture[i], 8);
			sidedef.texture[i] = texture[i];
			if(textureRemap.contains(sidedef.texture[i]))
				sidedef.texture[i] = textureRemap[sidedef.texture[i]];
		}
		stream >> sidedef.sector;

		d->sidedefList[WAD_NUM].append(sidedef);
	}
	closelump();
}

void MapDiff::loadThings()
{
	QSet<Thing> thingSets[2];

	openlump(Thing thing, 10, "THINGS", stream)
	{
		thing.num = STRUCT_NUM;
		stream >> thing.x >> thing.y >> thing.angle >> thing.doomed >> thing.flags;

		d->thingList[WAD_NUM].append(thing);
		thingSets[WAD_NUM].insert(thing);
	}
	closelump();

	d->addedThings = thingSets[1] - thingSets[0];
	d->removedThings = thingSets[0] - thingSets[1];
}

void MapDiff::loadVertexes()
{
	QSet<Vertex> vertexSets[2];

	openlump(Vertex vertex, 4, "VERTEXES", stream)
	{
		vertex.num = STRUCT_NUM;
		stream >> vertex.x >> vertex.y;

		d->vertexList[WAD_NUM].append(vertex);
		vertexSets[WAD_NUM].insert(vertex);

		// Update the bounds
		if(vertex.x < d->upperLeft.x())
			d->upperLeft.setX(vertex.x);
		if(vertex.y < d->upperLeft.y())
			d->upperLeft.setY(vertex.y);
		if(vertex.x > d->lowerRight.x())
			d->lowerRight.setX(vertex.x);
		if(vertex.y > d->lowerRight.y())
			d->lowerRight.setY(vertex.y);
	}
	closelump();

	d->addedVertexes = vertexSets[1] - vertexSets[0];
	d->removedVertexes = vertexSets[0] - vertexSets[1];
}
