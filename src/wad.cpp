#include "wad.h"

#include <QDataStream>
#include <QList>

struct LumpData
{
public:
	quint32 num;
	quint32 pos;
	quint32 size;
	QString name;
};

struct WadFile::Private
{
public:
	quint32 numLumps;
	QList<LumpData> lumps;
};


struct WadLump::Private
{
public:
	QByteArray buffer;
	LumpData data;
};

////////////////////////////////////////////////////////////////////////////////

WadFile::WadFile(const QString &name) : QFile(name), d(new Private())
{
}

WadFile::~WadFile()
{
	delete d;
}

WadLump WadFile::lump(const QString &name)
{
	foreach(const LumpData &data, d->lumps)
	{
		if(data.name.compare(name) == 0)
		{
			seek(data.pos);
			return WadLump(read(data.size), data);
		}
	}

	LumpData nullData = { INT_MAX, INT_MAX, INT_MAX };
	return WadLump(QByteArray(), nullData);
}

WadLump WadFile::lump(const QString &name, const WadLump &after)
{
	for(unsigned int i = after.d->data.num;i < d->numLumps;++i)
	{
		const LumpData &data = d->lumps[i];

		if(data.name.compare(name) == 0)
		{
			seek(data.pos);
			return WadLump(read(data.size), data);
		}
	}

	LumpData nullData = { INT_MAX, INT_MAX, INT_MAX };
	return WadLump(QByteArray(), nullData);
}

bool WadFile::open(QIODevice::OpenMode mode)
{
	// Wads are binary files
	mode &= ~QIODevice::Text;
	if(!QFile::open(mode))
		return false;

	// Read wad file stuff
	if(mode & QIODevice::ReadOnly)
	{
		if(!readWadFile())
		{
			close();
			return false;
		}
	}
	return true;
}

bool WadFile::readWadFile()
{
	QDataStream ds(this);
	ds.setByteOrder(QDataStream::LittleEndian);

	// Four CC check
	{
		char fourcc[4];
		ds.readRawData(fourcc, 4);
		if(!((fourcc[0] == 'P' || fourcc[0] == 'I') && fourcc[1] == 'W' && fourcc[2] == 'A' && fourcc[3] == 'D'))
			return false;
	}

	quint32 dirOffset;
	ds >> d->numLumps >> dirOffset;
	ds.skipRawData(dirOffset - 12);

	for(unsigned int i = 0;i < d->numLumps;++i)
	{
		LumpData ld;
		char name[9];

		ld.num = i;
		ds >> ld.pos >> ld.size;
		ds.readRawData(name, 8);
		name[8] = 0;
		ld.name = name;

		d->lumps.append(ld);
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

WadLump::WadLump() : QBuffer(), d(NULL)
{
}

WadLump::WadLump(const QByteArray &data, const LumpData &ld) : QBuffer(), d(new Private())
{
	d->data = ld;
	d->buffer = data;
	setBuffer(&d->buffer);
}

WadLump::~WadLump()
{
	delete d;
}

const WadLump &WadLump::operator=(const WadLump &copy)
{
	delete d;
	d = new Private();
	*d = *copy.d;
	setBuffer(&d->buffer);
}

qint64 WadLump::size() const
{
	return d->data.size;
}

bool WadLump::valid() const
{
	return d && d->data.num != INT_MAX;
}
