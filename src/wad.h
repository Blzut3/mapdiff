#pragma once

#include <QBuffer>
#include <QFile>

class WadLump;

class WadFile : public QFile
{
public:
	WadFile(const QString &name);
	~WadFile();

	WadLump lump(const QString &name);
	WadLump lump(const QString &name, const WadLump &after);
	bool open(QIODevice::OpenMode mode);

private:
	friend class WadLump;

	bool readWadFile();

	struct Private;
	Private *d;
};

class WadLump : public QBuffer
{
public:
	WadLump();
	WadLump(const WadLump &copy) : QBuffer(), d(NULL) { operator=(copy); }
	~WadLump();

	qint64 size() const;
	bool valid() const;

	const WadLump &operator=(const WadLump &copy);
protected:
	friend class WadFile;

	WadLump(const QByteArray &data, const struct LumpData &ld);

private:
	struct Private;
	Private *d;
};
